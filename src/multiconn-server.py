# Lib to work with low level OS primitives for IO
# Multiplexing instead of threads of asyncio
import socket
import selectors
import types

HOST = '0.0.0.0'
PORT = 65432
MAX_RECV = 1024


class multiconn_select_server:
    def __init__(self, host=HOST, port=PORT):
        self.host = host
        self.port = port
        self.lsock = self.init_nonblocking_server()
        self.sel = self.init_multiplex_io(self.lsock)
        self.run_server()

    def init_nonblocking_server(self):
        listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_sock.bind((HOST, PORT))
        listen_sock.listen()
        print('listening on', (self.host, self.port))
        listen_sock.setblocking(False)
        return listen_sock

    def init_multiplex_io(self, listener_sock):
        select_io = selectors.DefaultSelector()
        select_io.register(listener_sock, selectors.EVENT_READ, data=None)
        return select_io

    def run_server(self):
        while True:
            # blocks until there are remote sockets ready for I/O
            events = self.sel.select(timeout=None)
            for key, mask in events:
                if self.is_incoming_connection(key):
                    self.accept_connection(key)
                else:
                    self.service_connection(key, mask)

    def is_incoming_connection(self, key):
        return key.data is None

    def accept_connection(self, key):
        remote_sock = key.fileobj
        self.accept_wrapper(remote_sock)

    def accept_wrapper(self, sock):
        """ since listening socket was registered for event 
            selectors.EVENT_READ it should be ready to read
        """
        conn, addr = sock.accept()  
        print('accepted connected from', addr)
        conn.setblocking(False)    # Ensure the server does not block
        data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self.sel.register(conn, events, data=data)

    def service_connection(self, key, mask):
        remote_sock = key.fileobj
        data = key.data
        if self.is_read_event(mask):
            self.handle_read(remote_sock, data)
        if self.is_write_event(mask):
            self.handle_write(remote_sock, data)

    def is_read_event(self, mask):
        return mask & selectors.EVENT_READ

    def is_write_event(self, mask):
        return self.is_read_event(mask)

    def handle_read(self, remote_sock, data):
            recv_data = remote_sock.recv(MAX_RECV)
            if recv_data:
                data.outb += recv_data
            else:  
                # if 0 bytes recvd client closed connection
                print('closing connection to', data.addr)
                self.sel.unregister(remote_sock)
                remote_sock.close()

    def handle_write(self, remote_sock, data):
        if data.outb:
            print('echoing', repr(data.outb), 'to', data.addr)
            sent_bytes = remote_sock.send(data.outb)
            data.outb = data.outb[sent_bytes:]


if __name__ == '__main__':
    multiconn_select_server()
